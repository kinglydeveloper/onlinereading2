var express = require("express");
var app = express();
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var passport = require("passport");
var localStrat = require("passport-local");
var User = require("./models/user");
var fileUpload = require('express-fileupload');
var Books = require("./models/book");
var TempBooks = require("./models/tempbook");
var PrivateBooks = require("./models/pbook");
var EPub = require("epub");
var libgen = require("libgen");
var server = require('http').Server(app);
var methodOverride = require("method-override");
var compression = require("compression");
var isbn = require('node-isbn');


//Routes
var bookRoutes = require("./routes/books");
var dashboardRoutes = require("./routes/dashboard");
var authRoutes = require("./routes/auth");
var indexRoutes = require("./routes/index");

app.use(bodyParser.urlencoded({
    extended: true
}));

//mongoose.connect("mongodb://localhost/books");
mongoose.connect("mongodb://kody:3.14159265359@ds117830.mlab.com:17830/onlinereading", {
    useMongoClient: true
});

app.use(require("express-session")({
    secret: "Knowledge is Power",
    resave: false,
    saveUninitialized: false
}));
app.use(compression());
app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(passport.initialize());
app.use(passport.session());
app.use(fileUpload());
app.use(methodOverride("_method"));

passport.use(new localStrat(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    next();
});

//outside routes
app.use(bookRoutes);
app.use(dashboardRoutes);
app.use(authRoutes);
app.use(indexRoutes);


//Post Route for Adding a Book
app.post("/addbook", function(req, res) {
    //Upload book cover and epub
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    // Names of the files and the actual files themselves to be uploaded
    var fileName = req.body.isbn + ".epub";
    var imageName = req.body.isbn + ".jpg";
    var coverFile = req.files.coverFile;
    var bookFile = req.files.bookFile;

    // Use the mv() method to place the file somewhere on your server
    // if(req.body.private == "False"){
    //   coverFile.mv('public/uploads/covers/' + imageName, function(err) {
    //       if (err)
    //           return res.status(500).send(err);
    //   });
    //   bookFile.mv('public/bib/bookshelf/' + fileName, function(err) {
    //       if (err)
    //           return res.status(500).send(err);
    //   });
    // }else{
      coverFile.mv('public/uploads/covers/' + imageName, function(err) {
          if (err)
              return res.status(500).send(err);
      });
      bookFile.mv('public/bib/bookshelf/' + fileName, function(err) {
          if (err)
              return res.status(500).send(err);
      });
    //Files Upload, Hopefully
    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy;
    

    var isbn = req.body.isbn;
    var title = req.body.title;
    var author = req.body.author;
    var genre = req.body.genre;
    var fiction = req.body.fiction;
    var date = today;
    var desc = req.body.desc;
    var poster = req.user.username;
    var rating = req.body.rating;
    var pageCount = req.body.pageCount;
        //Create new book and save.
        // if(req.body.private == "True"){
        //   PrivateBooks.create({
        //       isbn: isbn,
        //       name: title,
        //       author: author,
        //       genre: genre,
        //       fileName: req.body.fileName,
        //       imageName: req.body.imageName,
        //       fiction: fiction,
        //       date: date,
        //       desc: desc,
        //       poster: poster
        //   }, function(err, book) {
        //       if (err) {
        //           console.log("Upload Failed!!!");
        //       } else {
        //           console.log("A book was created!");
        //           res.redirect("/all");
        //       }
        //   });
        // }else{
          Books.create({
              isbn: isbn,
              name: title,
              author: author,
              genre: genre,
              fileName: fileName,
              imageName: imageName,
              fiction: fiction,
              date: date,
              desc: desc,
              poster: poster,
              rating: rating,
              pageCount: pageCount
          }, function(err, book) {
              if (err) {
                  console.log("Upload Failed!!!.");
              } else {
                  console.log("A book was created!");
                  res.redirect("/all");
              }
          });
});


//Post Route for Adding a Book
app.post("/upload", function(req, res) {
        //Create new book and save.
          TempBooks.create({
              isbn: req.body.isbn
          }, function(err, tempbook) {
              if (err) {
                  console.log("Upload Failed!!!.");
              } else {
                  console.log("A isbn was created!");
                  res.redirect("/addbook2/" + req.body.isbn);
              }
          });
});

// libgen.mirror(function(err,urlString){
//   if (err)
//     return console.error(err);
//   return console.log(urlString + ' is currently fastest');
// });


server.listen(process.env.PORT || 5000, function() {
    console.log("Website Online. Reading time. By Illuminatiiiiii.");
});
