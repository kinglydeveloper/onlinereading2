var express = require("express");
var router = express.Router();
var Books = require("../models/book");
var User = require("../models/user");

//Counting For Genres
var fiction;
Books.count({
    "fiction": "Fiction"
}, function(err, count) {
    if (err) {
        console.log("User count failed to load.");
    } else {
        fiction = count;
    }
});
var nonfiction;
Books.count({
    "fiction": "Nonfiction"
}, function(err, count) {
    if (err) {
        console.log("User count failed to load.");
    } else {
        nonfiction = count;
    }
});
var horror;
Books.count({
    "genre": "Horror"
}, function(err, count) {
    if (err) {
        console.log("Count failed.");
    } else {
        horror = count;
    }
});
//End of Counting for Genres

var bookCount;
var userCount;

router.get("/", function(req, res) {

    Books.count({}, function(err, stats) {
        if (err) {
            console.log("Books count failed to load.");
        } else {
            bookCount = stats;
        }
    });
    User.count({}, function(err, count) {
        if (err) {
            console.log("User count failed to load.")
        } else {
            userCount = count;
        }
    });
    //Get All books from DB
    Books.find({}, function(err, allbooks) {
        if (err) {
            console.log("Problem getting all books");
        } else {
            res.render("index", {
                allbooks: allbooks,
                bookCount: bookCount,
                userCount: userCount,
                fiction: fiction,
                nonfiction: nonfiction,
                horror: horror
            });
        }
    }).sort('-date').limit(10);
});
// Contact Form Route
router.get("/contact", function(req, res) {
    res.render("contact");
});
//About Page
router.get("/about", function(req, res){
  res.render("about");
})
router.get("*", function(req, res) {
    res.render("404");
});

module.exports = router;
