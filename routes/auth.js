var express = require("express");
var router = express.Router();
var passport = require("passport");
var User = require("../models/user");

//AUTH ROUTES
//Login or Register Route
router.get("/login", function(req, res) {
    res.render("loginregister");
});
router.post("/login", passport.authenticate("local", { successRedirect: "/", failureRedirect: "/login" }), function(req, res) {
    //You should be logged in
});
router.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/");
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}
router.post("/register", function(req, res) {
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;
    User.register(new User({ username: username }), password, function(err, user) {
        if (err) {
            console.log(err);
            return res.render("loginregister");
        }
        passport.authenticate("local")(req, res, function() {
            res.redirect("/");
        });
    });
});

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}
// router.post("/llogin", function(req, res) {
    
// });


module.exports = router;
