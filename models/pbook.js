var mongoose = require("mongoose");
 //BOOK SCHEMA
var pbookSchema = new mongoose.Schema({
   isbn: String,
   name: String,
   author: String,
   genre : Array,
   imageName: String,
   fileName: String,
   desc: String,
   fiction: String,
   date: String,
   poster: String
});
module.exports = mongoose.model("PrivateBooks", pbookSchema);
